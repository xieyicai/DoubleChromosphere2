var datagrid_multiselect = $.extend($.fn.datagrid.defaults, {
	method: "post",
	iconCls:"icon-edit", //图标
	//height:400, //高度
	fitColumns: true, //自动调整各列，用了这个属性，下面各列的宽度值就只是一个比例。
	striped: true, //奇偶行颜色不同
	collapsible: true,//可折叠
	remoteSort: true, //服务器端排序
	idField: 'id', //主键字段
	pagination:true, //显示分页
	rownumbers:true, //显示行号
	pageList:[10,20,30,50,100,500,1000],//选择一页显示 行
	pageSize: 20,
	singleSelect:false,
	checkOnSelect:false,
	selectOnCheck:false,
	onSelect:function(rowIndex, rowData){
		var grid = $(this);
		var now = new Date().getTime();
		var last = grid.data("last_event_time");
		if(!last){
			last=0;
		}
		if(now - last > 100){
			grid.data("last_event_time", now);
			grid.datagrid("clearSelections");
			grid.datagrid("clearChecked");
			grid.datagrid('selectRow',rowIndex);
			grid.datagrid('checkRow',rowIndex);
		}
	},
	onUnselect:function(rowIndex, rowData){
		var grid = $(this);
		var now = new Date().getTime();
		var last = grid.data("last_event_time");
		if(!last){
			last=0;
		}
		if(now - last > 100){
			grid.data("last_event_time", now);
			grid.datagrid('uncheckRow',rowIndex);
		}
	}, 
	onCheck:function(rowIndex, rowData){
		var grid = $(this);
		var now = new Date().getTime();
		var last = grid.data("last_event_time");
		if(!last){
			last=0;
		}
		if(now - last > 100){
			grid.data("last_event_time", now);
			grid.datagrid('selectRow',rowIndex);
		}
	},
	onUncheck:function(rowIndex, rowData){
		var grid = $(this);
		var now = new Date().getTime();
		var last = grid.data("last_event_time");
		if(!last){
			last=0;
		}
		if(now - last > 100){
			grid.data("last_event_time", now);
			grid.datagrid('unselectRow',rowIndex);
		}
	},
	onCheckAll:function(rows){
		var grid = $(this);
		var now = new Date().getTime();
		var last = grid.data("last_event_time");
		if(!last){
			last=0;
		}
		if(now - last > 100){
			grid.data("last_event_time", now);
			grid.datagrid('selectAll');
		}
	},
	onUncheckAll:function(rows){
		var grid = $(this);
		var now = new Date().getTime();
		var last = grid.data("last_event_time");
		if(!last){
			last=0;
		}
		if(now - last > 100){
			grid.data("last_event_time", now);
			grid.datagrid('unselectAll');
		}
	}
});

