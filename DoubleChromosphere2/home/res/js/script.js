var fm={
	/**
	 * 将时间值格式化为形如“YYYY-MM-DD HH24-MI:SS”的字符串。
	 * @param value	 the new time in UTC milliseconds from the epoch.
	 */
	formatTime: function(value){
		if(value==null){
			return "";
		}
		var date = new Date(value);
		var str=date.getFullYear()+"-";
		var mm=date.getMonth()+1;
		if(mm<10){
			str+="0";
		}
		str+=mm+"-";
		var dd=date.getDate();
		if(dd<10){
			str+="0";
		}
		str+=dd+" ";
		var hh=date.getHours();
		if(hh<10){
			str+="0";
		}
		str+=hh+":";
		var mi=date.getMinutes();
		if(mi<10){
			str+="0";
		}
		str+=mi+":";
		var ss=date.getSeconds();
		if(ss<10){
			str+="0";
		}
		str+=ss;
		return str;
	}
	,getHTML: function(value){
		if(value==null){
			value="";
		}else if(typeof(value)==="string"){
			value = value.replace(new RegExp("\\&", "g"), "&amp;").replace(new RegExp("\\<", "g"),"&lt;").replace(new RegExp("\\>", "g"),"&gt;").replace(new RegExp("\\r\\n", "g"),"<br/>").replace(new RegExp("\\n", "g"),"<br/>");
		}
		return value;
	}
	,getSet: function(rows, fieldName){
		var set=[], value=null;
		if(rows && rows.length>0){
			for(var r=0; r<rows.length; r++){
				value=rows[r][fieldName];
				if(value>0){
					for(k=set.length-1; k>-1; k--){
						if(value==set[k]){
							break;
						}
					}
					if(k===-1){
						set.push(value);
					}
				}
			}
		}
		return set;
	}
	,fixData: function(rows, valueField, idField, url, onSuccess){
		var set = fm.getSet(rows, valueField);
		if(set.length>0){
			$.ajax({
				url: url,
				type: "POST",
				dataType: "json",
				timeout: 20000,
				async: "true",
				data: { ids: set.join(",") } ,
				error: function(jqXHR, textStatus, errorThrown){
					$.messager.alert("操作提示", "请求失败。"+errorThrown, "error");
				},
				success: function(rrr){
					if(rrr.success){
						var data=rrr.data;
						for(var m=0; m<data.length; m++){
							var id=data[m][idField];
							for(var r=0; r<rows.length; r++){
								if(rows[r][valueField]==id){
									rows[r][valueField+"_object"] = data[m];
								}
							}
						}
						if(onSuccess) onSuccess(data);
					}else{
						$.messager.alert("操作提示", rrr.msg, "error");
					}
				}
			});
		}
	}
	,load: function(url, params, successFunc){
		$.messager.progress({title:"提示信息", msg:"正在加载数据，请稍后...", interval:300});
		try{
			$.ajax({url: url,
				data: params,
				type: "POST",
				contentType: "application/x-www-form-urlencoded; charset=UTF-8",
				dataType: "json",
				timeout: 10000,
				async: true,
				complete: function(xmlHttpRequest, textStatus){
					$.messager.progress("close");
				},
				error: function(jqXHR, textStatus, errorThrown){
					$.messager.alert("操作提示", "请求失败。"+errorThrown, "error");
				},
				success: function(jj){
					if(jj && jj.length){
						successFunc(jj);
					}else if(jj.success){
						successFunc(jj);
					}else{
						$.messager.alert("操作提示", jj.msg, "error");
					}
				}
			});
		}catch(err){
			$.messager.progress("close");
			$.messager.alert("操作提示", "程序出错。\r\n"+err, "error");
		}
	}
	,getQueryParams: function(form){
		var query={}, value=null;
		var array = form.formToArray();
		for(var k=0; k<array.length; k++){
			value = query[array[k].name];
			if(value==null){
				value = array[k].value;
			}else if($.isArray(value)){
				value.push(array[k].value);
			}else{
				var temp = [];
				temp.push(value);
				temp.push(array[k].value);
				value = temp;
				temp=null;
			}
			query[array[k].name]=value;
		}
		for(name in query){
			var value=query[name];
			if(value==null || value.length==0){
				//query[name]=undefined;
				delete query[name];
			}else if($.isArray(value)){
				query[name]=value.join(",");
			}
		}
		return query;
	}
};

$.extend($.fn.validatebox.defaults.rules, {
	equalsLength: {
		validator: function(value, param){
			return value.length == param[0];
		},
		message: '输入的长度必须是 {0}.'
	}
	,number: {
		validator: function(value, param){
			if(new RegExp('^\\d+$', 'g').test(value)){
				if (value.length < param[0] || value.length > param[1]) {
					$.fn.validatebox.defaults.rules.number.message = '长度必须在' + param[0] + '至' + param[1] + '之间。';
				}else{
					return true;
				}
			}else{
				$.fn.validatebox.defaults.rules.number.message = '只能是数字。';
			}
			return false;
		}
		, message: '请输入合适长度的数字。'
	}
});
