/* ******************************************************************************
 * Test01.java
 * 创建日期:2017年12月29日
 * Copyright 2017 kingter company, Inc. All rights reserved.
 * xxxx PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *******************************************************************************/
package com.ball.tools;

import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Statement;

import javax.sql.DataSource;

import com.ball.tools.beans.Combination;
import com.ball.tools.util.Conf;
import com.ball.tools.util.Func;

/**
 * <p>标题: Test01</p>
 * <p>版权: Copyright (c) 2017</p>
 * <p>公司: xxxx</p>
 * <p>描述: </p>
 * 
 * @version 1.0
 * @author xieyicai
 */
public class Test01 {

	/**
	 * 
	 */
	public Test01() {
	}
	/**
	 * 罗列字头比
	 */
	public static String list(){
		StringBuilder buf=new StringBuilder(2048);
		for(int a=0; a<7; a++){
			for(int b=0; b<7; b++){
				for(int c=0; c<7; c++){
					for(int d=0; d<4; d++){
						if(a+b+c+d==6){
							buf.append(a).append(":").append(b).append(":").append(c).append(":").append(d).append("\r\n");
						}
					}
				}
			}
		}
		return buf.toString();
	}
	public static String list2(){
		StringBuilder buf=new StringBuilder(2048);
		for(int a=6; a>-1; a--){
			String str1=String.valueOf(a);
			int sum=a;
			for(int b=6-sum; b>-1; b--){
				sum+=b;
				String str2 = str1+":"+b;
				for(int c=6-sum; c>-1; c--){
					sum+=c;
					String str3 = str2+":"+c;
					for(int d=6-sum; d>-1; d--){
						sum+=d;
						String str4 = str3+":"+d;
						buf.append(str4);
						buf.append("\r\n");
					}
				}
			}
		}
		return buf.toString();
	}
	public static void getRadio(String prefix, int sum){
		if(sum>5){
			System.out.println(prefix);
		}else{
			int left=7-sum;
			for(int x=1; x<left; x++){
				getRadio(prefix+":"+x, sum+x);	//递归
			}
		}
	}
	public static void generate() throws Exception {
		Conf conf = Conf.getInstance(new File("conf/conf.xml"));
		//System.out.println(com.alibaba.fastjson.JSON.toJSONString(conf, true));
		DataSource ds = conf.getDefaultDataSource();
		Connection conn=null;
		PreparedStatement stmt=null;
		int count=0, sum=0;
		try{
			conn = ds.getConnection();
			stmt = conn.prepareStatement("insert into t_combination(a,b,c,d,e,f,sum_value)values(?,?,?,?,?,?,?)");
			for(int a=1; a<29; a++){
				for(int b=a+1; b<30; b++){
					for(int c=b+1; c<31; c++){
						for(int d=c+1; d<32; d++){
							for(int e=d+1; e<33; e++){
								for(int f=e+1; f<34; f++){
									count++;
									if(count%10000==0){
										System.out.print("\r"+count+"\t");
										stmt.executeBatch();
									}
									/*
									if(count>1107000){
										System.out.println(a+", "+b+", "+c+", "+d+", "+e+", "+f);
									}
									*/
									sum = a+b+c+d+e+f;
									stmt.setInt(1, a);
									stmt.setInt(2, b);
									stmt.setInt(3, c);
									stmt.setInt(4, d);
									stmt.setInt(5, e);
									stmt.setInt(6, f);
									stmt.setInt(7, sum);
									stmt.addBatch();
								}
							}
						}
					}
				}
			}
			if(count%10000!=0){
				System.out.print("\r"+count+"\t");
				stmt.executeBatch();
			}
		}finally{
			if(stmt!=null){
				stmt.close();
				stmt=null;
			}
			if(conn!=null){
				conn.close();
				conn=null;
			}
		}
		System.out.println("\r\n\r\ncount="+count);
	}
	public static void updateInfo() throws Exception {
		int[][] grid=new int[6][6];
		Conf conf = Conf.getInstance(new File("conf/conf.xml"));
		DataSource ds = conf.getDefaultDataSource();
		Connection conn1=null;
		Connection conn2=null;
		Statement stmt1=null;
		PreparedStatement stmt2=null;
		ResultSet rs=null;
		int count=0, seg=1;
		try{
			conn1 = ds.getConnection();
			conn2 = ds.getConnection();
			stmt2 = conn2.prepareStatement("update t_combination set ratio1=?, ratio2=?, ratio3=?, ratio4=?, series=?, same_tail=?, rowset=?, colset=?, join_style=? where id=?");
			stmt1 = conn1.createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			while(seg>0){
				seg=0;
				rs = stmt1.executeQuery("select * from t_combination where ratio1 is null limit 10000");
				while(rs.next()){
					count++;
					seg++;
					Combination bean = new Combination(rs);
					int[] array = bean.getArray(null);
					bean.setRatio1(Func.getRatio1(array));
					bean.setRatio2(Func.getRatio2(array));
					bean.setRatio3(Func.getRatio3(array));
					bean.setRatio4(Func.getRatio4(array));
					bean.setSeries(Func.getSeries(array));
					bean.setSameTail(Func.getSameTail(array));
					int[] rowcol=Func.getRowCol(array);
					bean.setRowset(rowcol[0]);
					bean.setColset(rowcol[1]);
					bean.setJoinStyle(Func.getJoinStyle(array, grid));
					int cc=0;
					stmt2.setString(++cc, bean.getRatio1());
					stmt2.setString(++cc, bean.getRatio2());
					stmt2.setString(++cc, bean.getRatio3());
					stmt2.setString(++cc, bean.getRatio4());
					stmt2.setString(++cc, bean.getSeries());
					stmt2.setString(++cc, bean.getSameTail());
					stmt2.setInt(++cc, bean.getRowset());
					stmt2.setInt(++cc, bean.getColset());
					stmt2.setInt(++cc, bean.getJoinStyle());
					stmt2.setInt(++cc, bean.getId());
					stmt2.addBatch();
				}
				if(seg!=0){
					System.out.print("\r"+count+"\t");
					stmt2.executeBatch();
				}
			}
		}finally{
			if(rs!=null){
				rs.close();
				rs=null;
			}
			if(stmt1!=null){
				stmt1.close();
				stmt1=null;
			}
			if(stmt2!=null){
				stmt2.close();
				stmt2=null;
			}
			if(conn1!=null){
				conn1.close();
				conn1=null;
			}
			if(conn2!=null){
				conn2.close();
				conn2=null;
			}
		}
		System.out.println("\r\n\r\ncount="+count);
	}
	public static void moveData() throws Exception {
		Conf conf = Conf.getInstance(new File("conf/conf.xml"));
		Connection conn1=null;
		Connection conn2=null;
		Statement stmt1=null;
		PreparedStatement stmt2=null;
		ResultSet rs=null;
		int count=0, seg=1, lastId=0, cc=0;
		try{
			conn1 = conf.getDataSource("mysql").getConnection();
			conn2 = conf.getDataSource("h2").getConnection();
			stmt1 = conn1.createStatement();
			stmt2 = conn2.prepareStatement("insert into t_combination()values()");
			while(seg>0){
				seg=0;
				rs = stmt1.executeQuery("SELECT * FROM t_combination where id>"+lastId+" ORDER BY id limit 10000");
				ResultSetMetaData metaData = rs.getMetaData();
				int columnCount= metaData.getColumnCount();
				while(rs.next()){
					count++;
					seg++;
					lastId=rs.getInt("id");
					for(cc=1; cc<=columnCount; cc++){
						stmt2.setObject(cc, rs.getObject(cc));
					}
					stmt2.addBatch();
				}
				if(seg!=0){
					System.out.print("\r"+count+"\t");
					stmt2.executeBatch();
				}
			}
		}finally{
			if(rs!=null){
				rs.close();
				rs=null;
			}
			if(stmt1!=null){
				stmt1.close();
				stmt1=null;
			}
			if(stmt2!=null){
				stmt2.close();
				stmt2=null;
			}
			if(conn1!=null){
				conn1.close();
				conn1=null;
			}
			if(conn2!=null){
				conn2.close();
				conn2=null;
			}
		}
		System.out.println("\r\n\r\ncount="+count);
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		long begin=System.currentTimeMillis();
		try{
			updateInfo();
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("\r\n主线程结束，耗时:"+(System.currentTimeMillis()-begin)+" 毫秒。");
	}

}
