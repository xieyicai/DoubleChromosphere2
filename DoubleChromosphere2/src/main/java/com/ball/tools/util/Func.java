/* ******************************************************************************
 * Func.java
 * 创建日期:2018年1月1日
 * Copyright 2018 kingter company, Inc. All rights reserved.
 * xxxx PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *******************************************************************************/
package com.ball.tools.util;

/**
 * <p>标题: Func</p>
 * <p>版权: Copyright (c) 2018</p>
 * <p>公司: xxxx</p>
 * <p>描述: </p>
 * 
 * @version 1.0
 * @author xieyicai
 */
public class Func {

	/**
	 * 
	 */
	public Func() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * 获取大小比
	 * @param array	一组号码
	 * @return	比例描述
	 */
	public static String getRatio1(int[] array){
		int big=0, small=0;
		for(int k=0; k<array.length; k++){
			if(array[k]<17){
				small++;
			}else{
				big++;
			}
		}
		return big+":"+small;
	}
	/**
	 * 获取奇偶比
	 * @param array	一组号码
	 * @return	比例描述
	 */
	public static String getRatio2(int[] array){
		int ji=0, ou=0;
		for(int k=0; k<array.length; k++){
			if(array[k]%2==0){
				ou++;
			}else{
				ji++;
			}
		}
		return ji+":"+ou;
	}
	/**
	 * 获取三区比
	 * @param array	一组号码
	 * @return	比例描述
	 */
	public static String getRatio3(int[] array){
		int s1=0, s2=0, s3=0;
		for(int k=0; k<array.length; k++){
			if(array[k]<12){
				s1++;
			}else if(array[k]>22){
				s3++;
			}else {
				s1++;
			}
		}
		return s1+":"+s2+":"+s3;
	}
	/**
	 * 获取字头比
	 * @param array	一组号码
	 * @return	比例描述
	 */
	public static String getRatio4(int[] array){
		int s1=0, s2=0, s3=0, s4=0;
		for(int k=0; k<array.length; k++){
			if(array[k]<10){
				s1++;
			}else if(array[k]<20){
				s2++;
			}else if(array[k]<30){
				s3++;
			}else {
				s4++;
			}
		}
		return s1+":"+s2+":"+s3+":"+s4;
	}
	/**
	 * 连号
	 * @param array	一组号码
	 * @return	比例描述
	 */
	public static String getSeries(int[] array){
		int[] len=new int[3];
		int group=0;
		for(int k=1; k<6; k++){
			if(array[k-1]+1 == array[k]){
				len[group]++;
			}else{
				if(len[group]!=0){
					group++;
				}
			}
		}
		group=0;
		int max=0;
		for(int x=0; x<len.length; x++){
			if(len[x]==0){
				
			}else{
				group++;
				if(len[x]>max){
					max=len[x];
				}
			}
		}
		max++;
		return group+"."+max;
	}
	/**
	 * 同尾
	 * @param array	一组号码
	 * @return	比例描述
	 */
	public static String getSameTail(int[] array){
		int[] tail=new int[10];
		for(int k=0; k<array.length; k++){
			tail[array[k]%10]++;
		}
		StringBuilder sb=new StringBuilder();
		for(int t=0; t<tail.length; t++){
			if(tail[t]>1){
				sb.append('|').append(tail[t]).append(t);
			}
		}
		if(sb.length()!=0){
			sb.append('|');
		}
		return sb.toString();
	}
	/**
	 * 行列
	 * @param array	一组号码
	 * @return	长度为2的数组，第一个元素表示行的分布，第二个元素表示列的分布
	 */
	public static int[] getRowCol(int[] array){
		int[] result=new int[2];
		for(int value : array){
			value--;
			int rr=value/6;
			int cc=value-rr*6;
			result[0]|=1<<(5-rr);
			result[1]|=1<<(5-cc);
		}
		return result;
	}
	/**
	 * 获取形态
	 * @param array	一组号码
	 * @param grid	6x6的计算容器
	 * @return  最大的关联长度（1-6）
	 */
	public static int getJoinStyle(int[] array, int[][] grid){
		int value, rr=-1, cc=-1;
		//重置表格
		for(rr=0; rr<grid.length; rr++){
			for(cc=0; cc<grid[rr].length; cc++){
				grid[rr][cc]=0;
			}
		}
		//绘点
		for(int k=0; k<array.length; k++){
			value=array[k]-1;
			rr=value/6;
			cc=value-rr*6;
			grid[rr][cc]=1;
		}
		//打印
		//print(grid);
		//计算
		int len = infect(grid, rr, cc);
		if(len<3){
			for(int m=0; m<5; m++){
				int[] point = findPoint(grid);
				if(point==null){
					break;
				}else{
					int next = infect(grid, point[0], point[1]);
					if(next>len){
						len = next;
					}
					if(len>=3){
						break;
					}
				}
			}
		}
		return len;
	}
	private static int infect(int[][] grid, int row, int col){
		int count=0, y1=row-1, y2=row+2, x1=col-1, x2=col+2, x, y;
		if(y1<0){
			y1=0;
		}
		if(y2>6){
			y2=6;
		}
		if(x1<0){
			x1=0;
		}
		if(x2>6){
			x2=6;
		}
		//System.out.println("x1="+x1+", x2="+x2+", y1="+y1+", y2="+y2);
		for(y=y1; y<y2; y++){
			for(x=x1; x<x2; x++){
				//System.out.println("grid["+y+"]["+x+"]="+grid[y][x]);
				if(grid[y][x]==1){
					grid[y][x]=8;
					count+=1+infect(grid, y, x);	//递归
				}
			}
		}
		return count;
	}
	private static int[] findPoint(int[][] grid){
		for(int m=0; m<grid.length; m++){
			for(int t=0; t<grid[m].length; t++){
				if(grid[m][t]==1){
					return new int[]{m, t};
				}
			}
		}
		return null;
	}
	public static void print(int[][] grid){
		for(int m=0; m<grid.length; m++){
			for(int t=0; t<grid[m].length; t++){
				System.out.print(grid[m][t]+" ");
			}
			System.out.println();
		}
	}
	public static void testSeries(){
		System.out.println(getSeries(new int[]{1,2,3,4,5,6}));
		System.out.println(getSeries(new int[]{1,2,5,6,9,10}));
		System.out.println(getSeries(new int[]{1,2,3,10,11,12}));
		System.out.println(getSeries(new int[]{1,2,3,4,5,12}));
		System.out.println(getSeries(new int[]{1,2,3,4,11,12}));
	}
	public static void testSameTail(){
		System.out.println(getSameTail(new int[]{1,2,3,4,5,6}));
		System.out.println(getSameTail(new int[]{1,2,5,6,9,10}));
		System.out.println(getSameTail(new int[]{1,2,3,10,11,12}));
		System.out.println(getSameTail(new int[]{1,2,3,4,5,12}));
		System.out.println(getSameTail(new int[]{1,2,3,4,11,12}));
	}
	public static void testRowCol(){
		int[] rowcol=getRowCol(new int[]{1,2,3,4,5,6});
		System.out.println(Integer.toBinaryString(rowcol[0])+", "+Integer.toBinaryString(rowcol[1]));
		rowcol=getRowCol(new int[]{5,7,8,9,24,30});
		System.out.println(Integer.toBinaryString(rowcol[0])+", "+Integer.toBinaryString(rowcol[1]));
		rowcol=getRowCol(new int[]{1,12,16,18,22,33});
		System.out.println(Integer.toBinaryString(rowcol[0])+", "+Integer.toBinaryString(rowcol[1]));
	}
	public static void testJoinStyle(){
		int[][] grid=new int[6][6];
		//System.out.println(getJoinStyle(new int[]{1,2,3,4,5,6}, grid));
		//System.out.println(getJoinStyle(new int[]{8,9,14,21,28,29}, grid));
		//System.out.println(getJoinStyle(new int[]{8,9,14,21,28,31}, grid));
		//System.out.println(getJoinStyle(new int[]{8,9,14,18,21,31}, grid));
		//System.out.println(getJoinStyle(new int[]{8,9,14,18,24,25}, grid));
		//System.out.println(getJoinStyle(new int[]{8,9,14,18,24,29}, grid));
		//System.out.println(getJoinStyle(new int[]{5,8,9,14,24,27}, grid));
		//System.out.println(getJoinStyle(new int[]{4,8,14,22,24,31}, grid));
		//System.out.println(getJoinStyle(new int[]{4,5,8,14,22,24}, grid));
		//System.out.println(getJoinStyle(new int[]{4,5,8,14,23,24}, grid));
		System.out.println(getJoinStyle(new int[]{4,6,8,19,24,27}, grid));
		//print(grid);
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		long begin=System.currentTimeMillis();
		try{
			//testSeries();
			//testSameTail();
			//testRowCol();
			testJoinStyle();
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("\r\n主线程结束，耗时:"+(System.currentTimeMillis()-begin)+" 毫秒。");
	}
}
