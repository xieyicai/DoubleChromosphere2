/* ******************************************************************************
 * Combination.java
 * 创建日期:2017年12月30日
 * Copyright 2017 kingter company, Inc. All rights reserved.
 * xxxx PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *******************************************************************************/
package com.ball.tools.beans;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * <p>标题: Combination</p>
 * <p>版权: Copyright (c) 2017</p>
 * <p>公司: xxxx</p>
 * <p>描述: </p>
 * 
 * @version 1.0
 * @author xieyicai
 */
public class Combination implements Serializable, Cloneable {

	/**  */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer a;
	private Integer b;
	private Integer c;
	private Integer d;
	private Integer e;
	private Integer f;
	private Integer sumValue;	//和值，最大值，183
	private String ratio1;	//大小比
	private String ratio2;	//奇偶比
	private String ratio3;	//三区比
	private String ratio4;	//字头比
	private String series;	//连号，'1.6','1.5','1.4','1.3','1.2','2.3','2.2','3.2'
	private String sameTail;	//同尾
	private Integer rowset;	//所在的行（按比特位处理）
	private Integer colset;	//所在的列（按比特位处理）
	private Integer joinStyle;	//形态
	private Integer divider;	//间距个数
	private Integer interval2;	//间隔长度

	/**
	 * 
	 */
	public Combination() {
	}
	
	public Combination(int a, int b, int c, int d, int e, int f) {
		setA(a);
		setB(b);
		setC(c);
		setD(d);
		setE(e);
		setF(f);
	}
	
	public Combination(ResultSet rs) throws SQLException {
		setId(getInteger(rs, "id"));
		setA(getInteger(rs, "a"));
		setB(getInteger(rs, "b"));
		setC(getInteger(rs, "c"));
		setD(getInteger(rs, "d"));
		setE(getInteger(rs, "e"));
		setF(getInteger(rs, "f"));
		setSumValue(getInteger(rs, "sum_value"));
		setRatio1(rs.getString("ratio1"));
		setRatio2(rs.getString("ratio2"));
		setRatio3(rs.getString("ratio3"));
		setRatio4(rs.getString("ratio4"));
		setSeries(rs.getString("series"));
		setSameTail(rs.getString("same_tail"));
		setRowset(getInteger(rs, "rowset"));
		setColset(getInteger(rs, "colset"));
		setJoinStyle(getInteger(rs, "join_style"));
		setDivider(getInteger(rs, "divider"));
		setInterval2(getInteger(rs, "interval2"));
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getA() {
		return a;
	}

	public void setA(Integer a) {
		this.a = a;
	}

	public Integer getB() {
		return b;
	}

	public void setB(Integer b) {
		this.b = b;
	}

	public Integer getC() {
		return c;
	}

	public void setC(Integer c) {
		this.c = c;
	}

	public Integer getD() {
		return d;
	}

	public void setD(Integer d) {
		this.d = d;
	}

	public Integer getE() {
		return e;
	}

	public void setE(Integer e) {
		this.e = e;
	}

	public Integer getF() {
		return f;
	}

	public void setF(Integer f) {
		this.f = f;
	}

	public Integer getSumValue() {
		return sumValue;
	}

	public void setSumValue(Integer sumValue) {
		this.sumValue = sumValue;
	}

	public String getRatio1() {
		return ratio1;
	}

	public void setRatio1(String ratio1) {
		this.ratio1 = ratio1;
	}

	public String getRatio2() {
		return ratio2;
	}

	public void setRatio2(String ratio2) {
		this.ratio2 = ratio2;
	}

	public String getRatio3() {
		return ratio3;
	}

	public void setRatio3(String ratio3) {
		this.ratio3 = ratio3;
	}

	public String getRatio4() {
		return ratio4;
	}

	public void setRatio4(String ratio4) {
		this.ratio4 = ratio4;
	}

	public String getSeries() {
		return series;
	}

	public void setSeries(String series) {
		this.series = series;
	}

	public String getSameTail() {
		return sameTail;
	}

	public void setSameTail(String sameTail) {
		this.sameTail = sameTail;
	}

	public Integer getRowset() {
		return rowset;
	}

	public void setRowset(Integer rowset) {
		this.rowset = rowset;
	}

	public Integer getColset() {
		return colset;
	}

	public void setColset(Integer colset) {
		this.colset = colset;
	}

	public Integer getJoinStyle() {
		return joinStyle;
	}

	public void setJoinStyle(Integer joinStyle) {
		this.joinStyle = joinStyle;
	}

	public Integer getDivider() {
		return divider;
	}

	public void setDivider(Integer divider) {
		this.divider = divider;
	}

	public Integer getInterval2() {
		return interval2;
	}

	public void setInterval2(Integer interval2) {
		this.interval2 = interval2;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	public int[] getArray(int[] array){
		if(array==null){
			array=new int[6];
		}
		array[0]=a;
		array[1]=b;
		array[2]=c;
		array[3]=d;
		array[4]=e;
		array[5]=f;
		return array;
	}
	public static Integer getInteger(ResultSet rs, String colName) throws SQLException {
		Object obj = rs.getObject(colName);
		if(obj==null){
			return null;
		}else if(obj instanceof Integer){
			return (Integer)obj;
		}else if(obj instanceof Long){
			return ((Long)obj).intValue();
		}else if(obj instanceof java.math.BigDecimal){
			return ((java.math.BigDecimal)obj).intValue();
		}else if(obj instanceof Byte){
			return ((Byte)obj).intValue();
		}else if(obj instanceof Short){
			return ((Short)obj).intValue();
		}
		throw new IllegalArgumentException("数据类型不匹配。"+obj.getClass().getName());
	}
}
