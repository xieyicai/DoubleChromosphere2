/* ******************************************************************************
 * MyNanolets.java
 * 创建日期:2017年12月28日
 * Copyright 2017 kingter company, Inc. All rights reserved.
 * xxxx PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 *******************************************************************************/
package com.ball.tools;

import java.io.File;

import org.nanohttpd.router.RouterNanoHTTPD;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.ball.tools.handlers.FileHandler;

/**
 * <p>标题: MyNanolets</p>
 * <p>版权: Copyright (c) 2017</p>
 * <p>公司: xxxx</p>
 * <p>描述: </p>
 * 
 * @version 1.0
 * @author xieyicai
 */
public class MyNanolets extends RouterNanoHTTPD {
	/** 日志记录器 */
	public static final Logger LOG = LoggerFactory.getLogger(MyNanolets.class);
	/**
	 * 
	 */
	public MyNanolets(int port) {
		super(port);
		addMappings();
	}
	@Override
	public void addMappings() {
		try{
			super.addMappings();
			addRoute("(.)*", FileHandler.class, new File("home").getCanonicalFile());
		}catch(Exception ex){
			LOG.error("处理器初始化失败。", ex);
		}
	}
}
